/**
 * Created by awk on 23/07/14.
 */

var request = require('request');


var irc = require('irc');
var cheerio = require('cheerio');
var _ = require('underscore');

var Datastore = require('nedb')
var explainDb = new Datastore({ filename: 'explainDb', autoload: true });
explainDb.ensureIndex({ fieldName: 'key', unique: true });

var result = {p: -1, o: -1};
//var channels = ['##periwinkletest','##orangeredtest'];
//var nick = 'botastest';
var channels = ['#periwinkle','##orangered'];
var nick = 'botas';



var total = 1852.0;

var userCount= {};


var ircClient = new irc.Client('irc.freenode.net',nick,{channels:channels, debug: true});

//function sendJoke() {
//    request('http://api.icndb.com/jokes/random', function(error,response,text) {
//        if (!error && response.statusCode == 200) {
//            var joke = JSON.parse(text).value.joke;
//            ircClient.say(channel, joke);
//        }
//
//    });
//}
function sendPerc(channel) {
    ircClient.say(channel,"Remaining: " + (100-((result.p + result.o)/total*100.0)).toFixed(2) +'%');
}

//function sendHelp() {
////    ircClient.say(channel,"Triggers:")
//    ircClient.say(channel,"!score - Gives the current casualties")
//    ircClient.say(channel,"!perc - Percentage Remaining soldiers.");
//    ircClient.say(channel,"!backup - Request Immediate Backup");
//    ircClient.say(channel,"!joke - Displays a random joke.");
//    ircClient.say(channel,"!explain - Displays a random joke.");
//}

function sendResult(channel) {
    ircClient.say(channel,"Orangered " + result.o + ":" + result.p + " Periwinkle");

}

function triggerBackup(channel) {
    console.log("triggering backup for channel", channel);
    request.post("http://comingupnext.org/reddit/chat/triggerAlarm.php",{form: {channel: channel}});
}


function setExplain(text) {
    var match = /^!explain\s(\w+)\s(.+)/.exec(text);
    var key = match[1];
    var value = match[2];
    explainDb.remove({key: key});
    explainDb.insert({key: key, value: value});
}

function explain(text, channel) {
    var match = /^\?\?(\w+)/.exec(text);
    var key = match[1];
    explainDb.findOne({key: key}, function (err, doc) {
        if (doc) {
            ircClient.say(channel, doc.key + " - " + doc.value);

        } else {
            ircClient.say(channel, key + " is not defined yet");

        }

    });

}
ircClient.on('message#',function(nick,channel,text){
    if (/^!score/.test(text)) {
        sendResult(channel);
//    } else if (/^!joke/.test(text)) {
//        sendJoke();
    } else if (/^!perc/.test(text)) {
        sendPerc(channel);
//    } else if (/^!refresh/.test(text)) {
//        refresh();
    } else if (/^!backup/.test(text)) {
        triggerBackup(channel);
//    } else if (/^!help/.test(text)) {
//        sendHelp();
    } else if (/^!explain\s(\w+)\s(.+)/.test(text)) {
        setExplain(text);
    } else if (/^\?\?(\w+)/.test(text)) {
        explain(text,channel);
    }
});


ircClient.on('message', function(from,to,text) {
    if(text === '!kawabonga') {
        ircClient.disconnect('Restarting!', function() {
            process.exit();
        });
        setTimeout(process.exit, 5000);
    }
});

ircClient.on('error', function(error) {
    console.log("error", error);
});



function sendResultToAll() {
    _.forEach(channels,sendResult);
}
var refresh = function () {
    request('http://reddit.com/r/nofapwar', function (error, response, html) {
        if (!error && response.statusCode == 200) {
            var $ = cheerio.load(html);
            var newResult = {};
            newResult.o = (parseInt(/\d+/.exec($('.md ol li').first().text())));
            newResult.p = (parseInt(/\d+/.exec($('.md ol li').last().text())));
            if (!_.isEqual(result, newResult)) {
                result = newResult;
                sendResultToAll();
            }
        }

    })
};

function refreshUserCount() {
    _.each(channels,function(channel) {
        ircClient.list(channel);
    })
}

function postUpdatedUserCount() {
    if (!_.isEmpty(userCount)) {
        request.post("http://comingupnext.org/reddit/chat/userCounts.php", {form: userCount});
    }
}

ircClient.on('channellist', function(channelList) {
    console.log('got channel list', channelList);
    if (channelList[0]) {
        userCount[channelList[0].name] = parseInt(channelList[0].users);
    }
    console.log('userCount', userCount);
});

ircClient.on('registered', refresh);
ircClient.on('registered', refreshUserCount);
setInterval(refresh, 60000);
setInterval(refreshUserCount, 60000);
setInterval(postUpdatedUserCount, 60000);




